<?php
/*
Plugin Name: Order to Chaos
Plugin URI: http://janine.winters.design/
Version: 0.1
Author: Frankie
Description: Adds shortcode [order_to_chaos] featuring the Order to Chaos Effect™
Text Domain: order-to-chaos
*/

/**
 * Register jquery and style on initialization
 */
function wintersdesign_order_to_chaos_register_scripts() {
    wp_register_style( 'order-to-chaos', plugins_url('/order-to-chaos.css', __FILE__), false, '1.0.0', 'all');
    wp_register_style( 'animate-css', plugins_url('vendor/animate.min.css', __FILE__), false, '1.0.0', 'all');
}
add_action('init', 'wintersdesign_order_to_chaos_register_scripts');

/**
 * A count of category-order-to-chaos
 */
function wintersdesign_order_to_chaos_shortcode() {
   wp_enqueue_style('order-to-chaos');
   wp_enqueue_style('animate-css');
   $shortcode_HTML = '<p class="lede chaos"><span class="animated rollIn chaos-letter chaos-letter-1">I</span><span class="animated rotateInDownRight chaos-letter chaos-letter-2">&nbsp;</span><span class="animated bounceInUp chaos-letter chaos-letter-3">l</span><span class="animated fadeInLeftBig chaos-letter chaos-letter-4">o</span><span class="animated fadeInRightBig chaos-letter chaos-letter-5">v</span><span class="animated fadeInUpBig chaos-letter chaos-letter-6">e</span><span class="animated fadeInRightBig chaos-letter chaos-letter-7">&nbsp;</span><span class="animated zoomInUp chaos-letter chaos-letter-8">b</span><span class="animated slideInRight chaos-letter chaos-letter-9">r</span><span class="animated bounceInLeft chaos-letter chaos-letter-10">i</span><span class="animated fadeInUpBig chaos-letter chaos-letter-11">n</span><span class="animated fadeInLeftBig chaos-letter chaos-letter-12">g</span><span class="animated flipInY chaos-letter chaos-letter-13">i</span><span class="animated bounceInRight chaos-letter chaos-letter-14">n</span><span class="animated bounceInUp chaos-letter chaos-letter-15">g</span><span class="animated rotateInDownLeft chaos-letter chaos-letter-16">&nbsp;</span><br/><span class="animated flipInX chaos-letter chaos-letter-17">o</span><span class="animated bounceInDown chaos-letter chaos-letter-18">r</span><span class="animated lightSpeedIn chaos-letter chaos-letter-19">d</span><span class="animated zoomInDown chaos-letter chaos-letter-20">e</span><span class="animated fadeInRightBig chaos-letter chaos-letter-21">r</span><span class="animated zoomIn chaos-letter chaos-letter-22">&nbsp;</span><span class="animated rotateInUpRight chaos-letter chaos-letter-23">t</span><span class="animated fadeInDownBig chaos-letter chaos-letter-24">o</span><span class="animated rotateInUpLeft chaos-letter chaos-letter-22">&nbsp;</span><span class="animated rotateInUpRight chaos-letter chaos-letter-25">c</span><span class="animated rotateInDownLeft chaos-letter chaos-letter-26">h</span><span class="animated zoomInDown chaos-letter chaos-letter-27">a</span><span class="animated fadeInRightBig chaos-letter chaos-letter-28">o</span><span class="animated rollIn chaos-letter chaos-letter-29">s</span><span class="animated rotateInUpLeft chaos-letter chaos-letter-21">.</span></p>';
   return $shortcode_HTML;
}

add_shortcode( 'order_to_chaos', 'wintersdesign_order_to_chaos_shortcode' );
